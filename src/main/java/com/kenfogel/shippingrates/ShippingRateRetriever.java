package com.kenfogel.shippingrates;

import static java.nio.file.Files.newInputStream;
import static java.nio.file.Paths.get;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Retrieving the properties is in its own class as it performs a very specific
 * task
 *
 * @author Ken Fogel
 *
 */
public class ShippingRateRetriever {

    // You are not a real programmer until you use a Logger rather than 
    // System.out.println for diagnostic information
    private final static Logger LOG = LoggerFactory.getLogger(ShippingRateRetriever.class);

    /**
     * Retrieves the contents of a .properties file in a Properties object.
     *
     * @param path
     * @param propFileName
     * @return
     * @throws IOException
     */
    public final Properties loadShippingProperties(final String path, final String propFileName) throws IOException {

        Properties prop = new Properties();

        Path txtFile = get(path, propFileName + ".properties");

        // File must exist
        if (Files.exists(txtFile)) {
            try ( InputStream propFileStream = newInputStream(txtFile);) {
                prop.load(propFileStream);
            }
        }
        LOG.debug(prop.toString());
        return prop;
    }
}

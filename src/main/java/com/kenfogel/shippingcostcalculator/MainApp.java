package com.kenfogel.shippingcostcalculator;

import com.kenfogel.data.ShippingData;
import com.kenfogel.shippingrates.ShippingRateRetriever;
import java.io.IOException;
import java.util.Properties;
import java.util.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * First exercise in CEJV 569
 *
 * @author Ken Fogel
 */
public class MainApp {

    // You are not a real programmer until you use a Logger rather than 
    // System.out.println for diagnostic information
    private final static Logger LOG = LoggerFactory.getLogger(MainApp.class);

    private Properties shippingProperties = null;
    private final ShippingData shippingData;
    private final Scanner sc;
    
    private final static double ONE = 1.0;
    private final static double FIVE = 5.0;
    private final static double TWENTY = 20.0;

    /**
     * Constructor that initializes instance variables and loads the properties
     */
    public MainApp() {
        shippingData = new ShippingData();
        ShippingRateRetriever rateRetriever = new ShippingRateRetriever();
        try {
            shippingProperties = rateRetriever.loadShippingProperties("", "shipping_data");
        } catch (IOException ex) {
            LOG.error("Unable to retrive shipping data", ex);
            System.exit(1);
        }

        /**
         * Get the base rate from the properties. If the string from the
         * properties file is invalid then the unchecked exception
         * NumberFormatException will be thrown. There is no way to recover from
         * this so the program will crash.
         */
        shippingData.setBaseCost(Double.parseDouble(shippingProperties.getProperty("BASE_RATE")));

        sc = new Scanner(System.in);
        LOG.info("In the constructor");
    }

    /**
     * Input routine for getting the weight cost multiplier based on the
     * province the parcel is going to.
     */
    private void getProvinceMultiplier() {
        double multiplier = 0;

        do {
            System.out.println("Please enter the province abbreviation: ");
            // Don't trust the user to enter the string in all upper case
            String provinceAbbreviation = sc.nextLine().toUpperCase();
            // Let's get the multiplier, as a String, from the properties
            String multiplierStr = shippingProperties.getProperty(provinceAbbreviation);
            // If its not found then the String will be null and multiplier will remain 0.0
            if (multiplierStr != null) {
                try {
                    multiplier = Double.parseDouble(multiplierStr);
                } catch (NumberFormatException nfe) {
                    // Using the exception to recognize invalid input
                    multiplier = 0.0;
                }
            }
            // Oops, need to try again
            if (multiplier == 0) {
                System.out.println("Your input was invalid.");

            }
        } while (multiplier == 0); // Continue until they follow the instructions
        LOG.debug("multiplier = " + multiplier);
        // Put the valuse in our bean
        shippingData.setMultiplier(multiplier);
    }

    /**
     * Input routine for the parcel weight, accepts any positive value
     */
    public void getThePackageWeight() {
        double weight = 0.0;
        do {
            System.out.println("Please enter the package weight: ");
            if (sc.hasNextDouble()) {
                weight = sc.nextDouble();
            } 
            sc.nextLine();  // clear out the keyboard buffer
            if (weight <= 0.0) {
                System.out.print("The weight was invalid. ");
            }
        } while (weight <= 0.0);
        LOG.debug("weight = " + weight);
        shippingData.setWeight(weight);
    }

    /**
     * Questions user if they want to continue after a result is displayed
     *
     * @return true - new parcel false - finished
     */
    public boolean wantToContinue() {
        String choice = null;
        do {
            System.out.println("Would you like to continue y or n: ");
            // Will only accept the single letters as a String
            if (sc.hasNext("[YNyn]")) {
                choice = sc.next().toLowerCase();
            } 
            sc.nextLine();  // clear out the keyboard buffer
            if (choice == null) {
                System.out.print("Please only enter y or n. ");
            }
        } while (choice == null);
        LOG.debug("choice = " + choice);
        return (choice.equals("y"));
    }

    /**
     * Calculates the final cost.
     *
     * @return
     */
    private double calculateResult() {

        String weightCostStr = "0.0";
        if (shippingData.getWeight() <= ONE) {
            weightCostStr = shippingProperties.getProperty("ONE_KILO");
        } else {
            if (shippingData.getWeight() <= FIVE) {
                weightCostStr = shippingProperties.getProperty("FIVE_KILO");
            } else {
                if (shippingData.getWeight() <= TWENTY) {
                    weightCostStr = shippingProperties.getProperty("TWENTY_KILO");
                }
            }
        }
        double weightCost = 0.0;
        try {
            weightCost = Double.parseDouble(weightCostStr);
        } catch (NumberFormatException nfe) {
            // This will only happen if the properties file contains an invalid 
            // string for a value. This is a critical error so the program exits.
            LOG.error("Invalid number format", nfe);
            System.exit(1);
        }
        shippingData.setWeightCost(weightCost);

        double result = shippingData.getBaseCost()
                + (shippingData.getWeight() * shippingData.getWeightCost()
                * shippingData.getMultiplier());
        return result;
    }

    /**
     * This method runs the main loop of the program
     *
     */
    public void perform() {

        double result;

        do {
            getProvinceMultiplier();
            getThePackageWeight();
            if (shippingData.getWeight() > TWENTY) {
                // Using the new Text Block
                System.out.println("""
                                   Your parcel weighs more that the 20 kilo limit.
                                   We cannot ship your parcel.
                                   """);
            } else {
                result = calculateResult();
                System.out.println("Shipping cost will be: " + result);
            }
        } while (wantToContinue());
    }

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        MainApp mainApp = new MainApp();
        mainApp.perform();
    }
}
